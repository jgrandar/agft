/**
 * Creado por: Javier Granda Ruiz
 * Fecha: 27/04/2018 - 1:55
 * Fichero: index.js.js
 * Proyecto: agft
 * Editor: WebStorm.
 */

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    bets: [],
  },
  getters: {
    getBetStatusById: state => betId => state.bets.find(bet => bet.id === betId).selected,
    getActiveBets: state => state.bets.filter(bet => bet.selected),
  },
  mutations: {
    initBet(state, bet) {
      const finalBet = Object.assign({}, bet);
      finalBet.selected = false;
      state.bets.push(finalBet);
    },
    toggleBet(state, betId) {
      const bet = state.bets.find(item => item.id === betId);
      bet.selected = !bet.selected;
    },
    unselectBet(state, betId) {
      state.bets.find(bet => bet.id === betId).selected = false;
    },
  },
});
