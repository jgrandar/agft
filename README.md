# agft

> Addison Global Frontend Test

## Stack
This is my proposed solution to the task using the following stack:
- Vue.js (JavaScript framework)
- Vuex (State management)
- Axios (HTTP client)
- Vue-awesome (Icons).

I decided to use Vue because it is the JavaScript library / framework that I know best. I have not implemented any error management functionality or tests in order to save time.
Vuex is only used to manage Selections as they need communication between the component and the Betslip.
I didn't normalize the AJAX response data because is not necessary with this approach, I know normalize the data would be the wright way working with Redux.

After finished the Vue implemented task I have also tried to perform the same task based on React, more information here https://bitbucket.org/jgrandar/agft_react

## Running the app
I have included the /dist folder deliberately in the repository so you don't need to build it to run the app.
Built files are located at /dist folder, these files are meant to be served over an HTTP server, opening index.html over file:// won't work.
You need to run it over a web server, you can user XAMPP, LAMP, etc. or just lunch the PHP built in server from the command line:

```
cd path/to/dist/folder
php -S localhost:8080
```

